﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wanderer : FleetManager
{
    //
    //TO DO: rewrite into simpler and cleaner form
    //
    //This is Wanderer lifeform, lifeform of especially huge fleets that wander where ever their ships nose takes them,
    //diverting from their random path only when they require resources of energy. Very rarely splits fleets.
    bool targetLocked;

    float Xrot;
    float Yrot;

    private void Start()
    {
        Set();
    }

    private void Update()
    {
        //MOVEMENT BEHAVIOUR
        //rotation
        Xrot = Random.Range(-5f, 5f);
        Yrot = Random.Range(-5f, 5f);
        if (!targetLocked) transform.Rotate(Xrot, Yrot, 0 * Time.deltaTime);
        //movement
        if (energyStorage > 0)
        {
            energyStorage -= Time.deltaTime;
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }

        //MATERIAL USAGE
        if (materialStorage > 0)
        {
            materialStorage -= Time.deltaTime;
            assembly += Time.deltaTime;

            if (assembly >= HP)
            {
                spawnNew();
            }
        }
        else HP -= Time.deltaTime;
    }

    private void OnCollisionEnter(Collision col)
    {
        if (materialStorage < materialStorageCap) materialStorage += Time.deltaTime;
    }

    private void OnTriggerEnter(Collider trig)
    {
        if (materialStorage < materialStorageCap / 2 && !targetLocked)
        {
            transform.LookAt(trig.transform);
            targetLocked = true;
        }
    }

    private void spawnNew()
    {
        GameObject I = Instantiate(Resources.Load("lifeforms/Wanderer") as GameObject);
        I.transform.position = transform.position;
        I.transform.parent = transform.parent;
        //I.GetComponent<Wanderer>().materialStorage += materialStorage / 2;

        assembly = 0;
        materialStorage = materialStorage / 2;

    }

    // Use this for initialization
    //private void Awake()
    //{
    //    if (First)  //if first of its kind (not inheriting), random range these
    //    {
    //        Aggression = Random.Range(minAggression, maxAggression);
    //        StoredEnergyCap = Random.Range(minStoredEnergyCap, maxStoredEnergyCap);
    //        Strength = Random.Range(minStrength, maxStrength);  //represents firepower of one ship
    //        Sight = Random.Range(minSight, maxSight);   //represents sight capability of entire fleet
    //        Form = Random.Range(1, 1001);   //represents form of this lifeform. Even if there are multiple wanderers, their form might be very varied.
    //        FleetCohesion = Random.Range(minFleetCohesion, maxFleetCohesion);
    //    }

    //    Gathering = Random.Range(minGathering, maxGathering);   //affects both resource and energy gathering
    //    HealthCap = Random.Range(minHealthCap, maxHealthCap);
    //    Health = HealthCap; //health represents mass and overall technology in one ship
    //    Movement = Random.Range(minMovement, maxMovement);
    //    StoredEnergy = StoredEnergyCap; //always starts with energy cap at full
    //}

    //void Start() {
    //    name = "wanderer";
    //    transform.parent = GameObject.Find("LifeformContainer").transform;
    //    transform.eulerAngles = new Vector3(Random.Range(0, 181), Random.Range(0, 181), Random.Range(0, 181));

    //    transform.GetChild(0).GetComponent<SphereCollider>().radius = Sight;
    //}

    //// Update is called once per frame
    //void Update() {

    //    //lifeform movement
    //    if (StoredEnergy > 0)
    //    {
    //        transform.Translate(Vector3.forward * Movement / (Vector3.Distance(transform.position, Vector3.zero) / 2) * Time.deltaTime);
    //        StoredEnergy -= Time.deltaTime;
    //    }
    //    //energy generation from matter
    //    if(StoredEnergy <= StoredEnergyCap / 2 && StoredResources > 0)
    //    {
    //        StoredEnergy += Time.deltaTime;
    //        StoredResources -= Time.deltaTime;
    //    }

    //    //lifeform rotation
    //    int LeftOrRight = Random.Range(0, 2);
    //    transform.Rotate(LeftOrRight > 0 ? Vector3.left * Random.Range(0, 180) * Time.deltaTime : Vector3.right * Random.Range(0, 180) * Time.deltaTime);

    //    int UpOrDown = Random.Range(0, 2);
    //    transform.Rotate(UpOrDown > 0 ? Vector3.up * Random.Range(0, 180) * Time.deltaTime : Vector3.down * Random.Range(0, 180) * Time.deltaTime);

    //    //preventing exit from universe bounds
    //    if (transform.position.x > 250) { transform.position = new Vector3(-250, transform.position.y, transform.position.z); }
    //    if (transform.position.x < -250) { transform.position = new Vector3(250, transform.position.y, transform.position.z); }
    //    if (transform.position.y > 250) { transform.position = new Vector3(transform.position.x, -250, transform.position.z); }
    //    if (transform.position.y < -250) { transform.position = new Vector3(transform.position.x, 250, transform.position.z); }
    //    if (transform.position.z > 250) { transform.position = new Vector3(transform.position.x, transform.position.y, -250); }
    //    if (transform.position.z < -250) { transform.position = new Vector3(transform.position.x, transform.position.y, 250); }

    //    //for clearing railrenderer when closing edge/loop of the universe
    //    if ((transform.position.x > 249.9f || transform.position.x < -249.9f) || (transform.position.y > 249.9f || transform.position.y < -249.9f) || (transform.position.z > 249.9f || transform.position.z < -249.9f))
    //    {
    //        GetComponent<TrailRenderer>().Clear();
    //    }

    //    //lifeform resource consumption
    //    if (StoredResources > 0)      //using resouces to reproduction
    //    {
    //        StoredResources -= Time.deltaTime;
    //        FindObjectOfType<Universe>().recycledResources += Time.deltaTime;

    //        Assembly += Time.deltaTime;
    //    }
    //    if (Assembly >= Health)     //health used to measure how much needed to assemble a new ship
    //    {
    //        FleetSize++;
    //        Assembly = 0;
    //    }
    //    if (FleetSize > FleetCohesion)
    //    {
    //        SplitFleet(Form, Aggression, StoredEnergyCap, StoredResources / 2, Strength, Sight, FleetCohesion, FleetSize / 2);
    //    }
    //    if (StoredResources > 0 && Health < HealthCap)      //recovering from lack of resources
    //    {
    //        StoredResources -= Time.deltaTime;
    //        Health += Time.deltaTime;
    //    }

    //    //lifeform hunger
    //    if (StoredResources <= 0)
    //    {
    //        if (!transform.GetChild(0).GetComponent<Sight>().lockedOnResources) transform.LookAt(Vector3.zero);       //heads back towards galaxy to get resources in case far away
    //        Health -= Time.deltaTime;     //fleet starts to take degeneration damage from lack of resources.
    //    }
    //    if (Health <= 0)    //losing one if ships are available
    //    {
    //        if (FleetSize > 0)  //if ships left to destroy, destroy one
    //        {
    //            FleetSize--;
    //            Health = HealthCap;
    //        }
    //        else Destroy(gameObject);   //if last ship, fleet gets destroyed
    //    }
    //}

    //public void SightOnStarSystem(Vector3 receiveStarLoc)
    //{
    //    if(StoredResources < HealthCap * 10) transform.LookAt(receiveStarLoc);
    //}

    //private void OnTriggerStay(Collider other)
    //{
    //    if (other.GetComponent<StarSystem>() && other.GetComponent<StarSystem>().resources > HealthCap * 3 || StoredEnergy < StoredEnergyCap / 2)
    //    {
    //        StoredResources += Gathering;
    //        if(StoredEnergy < StoredEnergyCap) StoredEnergy += other.GetComponent<StarSystem>().energyOutput / Gathering;
    //        other.GetComponent<StarSystem>().resources -= Gathering;
    //        if (StoredResources < HealthCap * 10) OrbitStar(other.transform.position);
    //    }
    //}

    //private void OrbitStar(Vector3 receiveStarLoc)
    //{
    //    transform.LookAt(receiveStarLoc);
    //    transform.Translate(Vector3.left * Movement * Time.deltaTime);
    //}

    //private void SightOnLifeform()
    //{

    //}

    //private void SplitFleet(float inheritForm, float inheritAggression, float inheritEnergyCap, float inheritResources, float inheritStrength, float inheritSight, int inheritFleetCohesion, int inheritFleeetSize)
    //{
    //    StoredResources = StoredResources / 2;
    //    FleetSize = FleetSize / 2;

    //    GameObject L = Resources.Load("lifeforms/wandererOffspring") as GameObject;
    //    GameObject I = Instantiate(L) as GameObject;

    //    I.transform.position = transform.position;

    //    I.transform.GetComponent<Lifeform>().Form = inheritForm;
    //    I.transform.GetComponent<Lifeform>().FleetSize = inheritFleeetSize;
    //    I.transform.GetComponent<Lifeform>().Aggression = inheritAggression;
    //    I.transform.GetComponent<Lifeform>().StoredEnergyCap = inheritEnergyCap;
    //    I.transform.GetComponent<Lifeform>().StoredResources = inheritResources;
    //    I.transform.GetComponent<Lifeform>().Strength = inheritStrength;
    //    I.transform.GetComponent<Lifeform>().Sight = inheritSight;
    //    I.transform.GetComponent<Lifeform>().FleetCohesion = inheritFleetCohesion;
    //}

}
