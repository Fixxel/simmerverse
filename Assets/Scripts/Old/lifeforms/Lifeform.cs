﻿using UnityEngine;
using System.Collections.Generic;

public class Lifeform : MonoBehaviour
{
    public float assembly;

    public float energyStorageCap = 1000;
    public float energyStorage;

    public float HP = 50;

    public float materialStorageCap = 200;
    public float materialStorage;

    public float speed = 1;

    //public bool First;

    //public float minAggression;     //set in inspector
    //public float maxAggression;     //set in inspector

    //public float Aggression;        //inherited, might cause conflict (Form also affects)

    //public float minGathering;      //set in inspector
    //public float maxGathering;      //set in inspector
    //public float Gathering;

    //public float minHealthCap;      //set in inspector
    //public float maxHealthCap;      //set in inspector
    //public float HealthCap;
    //public float Health;            //comes from healthcap on start

    //public int FleetSize;           //everyone starts with 1 ship, might later add so it depends on the resources of origin system

    //public int minFleetCohesion;    //set in inspector
    //public int maxFleetCohesion;    //set in inspector
    //public int FleetCohesion;

    //public float minMovement;       //set in inspector
    //public float maxMovement;       //set in inspector
    //public float Movement;

    //public float Assembly;          //always starts at 0. Basically the work amount fleet has put into assembling a new ship

    //public float minStrength;       //set in inspector
    //public float maxStrength;       //set in inspector
    //public float Strength;

    //public float StoredResources;   //comes from place of origin or inherited
    //public float StoredEnergy;      //always starts with energy storage full

    //public float minStoredEnergyCap;   //set in inspector
    //public float maxStoredEnergyCap;   //set in inspector
    //public float StoredEnergyCap;

    //public float minSight;          //set in inspector
    //public float maxSight;          //set in inspector
    //public float Sight;

    //public float Form;              //inherited, might cause conflict (Aggression also affects)

    public void Set()
    {
        materialStorage = materialStorageCap;
        energyStorage = energyStorageCap;
    }
}
