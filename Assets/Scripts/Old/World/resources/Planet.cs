﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour {

    bool upOrDown;
    bool leftOrRight;

    float thisSpeed;
    float psDistance; //planet to star distance

    float orbitCircumference;
    float orbitTraveled;
    float orbitSmoothing;

    Vector3 MoveDir;

    private void Start()
    {
        thisSpeed = Random.Range(0.1f, 0.25f);
        //thisSpeed = Random.Range(10.1f, 10.25f);
        psDistance = Vector3.Distance(transform.position, transform.parent.position);

        upOrDown = GetComponentInParent<Star>().UpOrDown;
        leftOrRight = GetComponentInParent<Star>().LeftOrRight;

        transform.GetComponent<TrailRenderer>().Clear();

        //calculating circumference of the orbit (sphere)
        orbitCircumference = 2 * Mathf.PI * psDistance;
    }

    private void Update()
    {
        //orbitSmoothing = thisSpeed / orbitCircumference * Time.deltaTime;

        transform.LookAt(transform.parent.position);
        //recorrect orbit
        transform.Translate(psDistance < Vector3.Distance(transform.position, transform.parent.position) ? Vector3.forward * (Vector3.Distance(transform.position, transform.parent.position) - psDistance) * Time.deltaTime : Vector3.back * (psDistance - Vector3.Distance(transform.position, transform.parent.position)) * Time.deltaTime);
        transform.Translate(leftOrRight ? Vector3.left * thisSpeed / Vector3.Distance(transform.position, transform.parent.position) * Time.deltaTime : Vector3.right * thisSpeed / Vector3.Distance(transform.position, transform.parent.position) * Time.deltaTime);


        //goes up/down when first half of the orbit is being traveled
        //if(orbitTraveled <= orbitCircumference / 2) transform.Translate(Vector3.up * thisSpeed / orbitCircumference / orbitSmoothing * Time.deltaTime);

        //goes up/down when second half of the orbit is being traveled
        //else if(orbitTraveled > orbitCircumference / 2) transform.Translate(Vector3.down * thisSpeed / orbitCircumference / orbitSmoothing * Time.deltaTime);

        //adding to orbitTraveled distance that has been traveled this frame
        orbitTraveled += thisSpeed / Vector3.Distance(transform.position, transform.parent.position) * Time.deltaTime;

        //checking if new circle of the orbit begins
        if(orbitTraveled >= orbitCircumference) orbitTraveled = 0;



        //handling orbitsmooth
        
    }

}