﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSystem : MonoBehaviour {

    public bool LeftOrRight;
    public bool UpOrDown;

    float dist;
    public float thisSpeed;
    public float resources;
    public float energyOutput;

    public int StarCount;
    //
    //might just stick to resourcepile system and scrap this
    private void Start()
    {

        thisSpeed = Random.Range(0.1f, 0.5f);   //speed at which star system rotates around universe/galaxy core (is still affected by the distance from the core in the update)
        //thisSpeed = Random.Range(110.1f, 110.5f);   //speed at which star system rotates around universe/galaxy core
        LeftOrRight = Random.Range(0, 2) > 0 ? true : false;    //direction of the rotation
        UpOrDown = Random.Range(0, 2) > 0 ? true : false;    //direction of the rotation

        StarCount = Random.Range(1,3);

        if (StarCount == 1)
        {
            GameObject I = Instantiate(Resources.Load("resources/Star") as GameObject);
            I.transform.position = transform.position;
            I.transform.parent = transform;
            I.GetComponent<Star>().StarCount = StarCount;
        }
        else if(StarCount == 2){
            GameObject I1 = Instantiate(Resources.Load("resources/Star") as GameObject);
            I1.transform.position = transform.position;
            I1.transform.parent = transform;
            I1.GetComponent<Star>().StarCount = StarCount;

            GameObject I2 = Instantiate(Resources.Load("resources/Star") as GameObject);
            I2.transform.position = transform.position;
            I2.transform.parent = transform;
            I2.GetComponent<Star>().StarCount = StarCount;

            //I1.transform.position += new Vector3(0, 0, Random.Range(1f,3f));
            I2.transform.position += new Vector3(0, 0, Random.Range(-1f, 1f));
        }

        dist = Vector3.Distance(transform.position, Vector3.zero);  //for keeping distance from the core
    }

    private void Update()
    {
        transform.LookAt(Vector3.zero);
        transform.Translate(LeftOrRight ? Vector3.left * thisSpeed / Vector3.Distance(transform.position, Vector3.zero) * Time.deltaTime : Vector3.right * thisSpeed / Vector3.Distance(transform.position, Vector3.zero) * Time.deltaTime);
        transform.Translate(dist < Vector3.Distance(transform.position, Vector3.zero) ? Vector3.forward * thisSpeed / Vector3.Distance(transform.position, Vector3.zero) * Time.deltaTime : Vector3.back * thisSpeed / Vector3.Distance(transform.position, Vector3.zero) * Time.deltaTime);


        if(Input.GetKeyUp(KeyCode.F)){
            thisSpeed = thisSpeed*2f;
        }
        else if(Input.GetKeyUp(KeyCode.G)){
            thisSpeed = thisSpeed/2f;
        }
        //transform.RotateAround(Vector3.zero, Vector3.left, 2 / Vector3.Distance(transform.position, Vector3.zero) * Time.deltaTime);
    }
}