﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : StarSystem {

    public float StarRotationSpeed;

    public List<Mesh> StarMeshL = new List<Mesh>();

    //
    //might just stick to resourcepile system and scrap this
    // Use this for initialization
    void Start ()
    {
        GetComponent<MeshFilter>().mesh = StarMeshL[Random.Range(1, StarMeshL.Count)];

        //random rotation speed
        StarRotationSpeed = Random.Range(20, 35);

        int planets = Random.Range(0, Random.Range(1, Random.Range(4, 15)));
        
        //base distance of planet from star
        float minPDist = 0.2f;
        float maxPDist = 1f;

        if (StarCount == 1)
        {
            while (planets > 0)
            {
                GameObject I = Instantiate(Resources.Load("resources/Planet")) as GameObject;

                I.transform.parent = transform;
                I.transform.position = transform.position;
                I.transform.position += new Vector3(Random.Range(minPDist, maxPDist), 0, 0);

                minPDist += Random.Range(0.1f, 0.2f);
                maxPDist += Random.Range(minPDist, minPDist + 0.1f);

                planets--;
            }

        }

        //radius of the star system for interaction
        //doesnt always work because child hierarchy doesnt go with their instantiation order, gotta figure out another way to set this
        if(transform.childCount > 0) GetComponentInParent<SphereCollider>().radius = Vector3.Distance(transform.position, transform.GetChild(transform.childCount - 1).position);
    }

    private void Awake()
    {
        transform.GetComponent<TrailRenderer>().Clear();
    }

    void Update()
    {
        if(StarCount > 1)
        {
            transform.LookAt(transform.parent.position);
            if(LeftOrRight) transform.Translate(Vector3.left * Vector3.Distance(transform.position, transform.parent.position) / 2 * Time.deltaTime);
            else transform.Translate(Vector3.left * Vector3.Distance(transform.position, transform.parent.position) / 2 * Time.deltaTime);
        }

        transform.Rotate(Vector3.up * 10 * Time.deltaTime, Space.World);
    }
}
