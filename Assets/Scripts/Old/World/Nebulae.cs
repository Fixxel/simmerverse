﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nebulae : MonoBehaviour {

    int leftOrRight;
    int upOrDown;

	// Use this for initialization
	void Start () {
        leftOrRight = Random.Range(0,2);
        upOrDown = Random.Range(0,3);
    }
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(leftOrRight > 0 ? Vector3.left * 0.05f * Time.deltaTime : Vector3.right * 0.05f * Time.deltaTime);
        transform.Rotate(upOrDown > 1 ? Vector3.up * 0.05f * Time.deltaTime : Vector3.down * 0.05f * Time.deltaTime);   //if < 1 no up or down rotation
    }
}
