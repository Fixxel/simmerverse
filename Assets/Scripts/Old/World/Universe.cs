﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Universe : MonoBehaviour
{

    public float recycledResources;

    public int SysCount;
    public float MinRange;
    public float MaxRange;

    [System.Serializable]
    public class ScatteredResources
    {
        public float amount;
        public int maxAmount;
        public int increment;
    }

    public List<ScatteredResources> resourcesList = new List<ScatteredResources>();

    // Use this for initialization
    void Start()
    {
        //Todo: make better mechanism for system spawning, this causes box-like structure to the galaxy
        //maybe have random range be variables that get incremented each while-loop?
        float Ylimiter = 0.5f;

        while (SysCount > 0)
        {
            GameObject I = Instantiate(Resources.Load("resources/StarSystem"), transform) as GameObject;
            
            I.transform.position = new Vector3(Random.Range(-MinRange, MaxRange), Random.Range(-MinRange / Ylimiter, MaxRange / Ylimiter), Random.Range(-MinRange, MaxRange));    //core systems
            I.GetComponent<StarSystem>().resources += 200;
            
            MinRange += Random.Range(MinRange/SysCount/2, MinRange/SysCount/1.25f);
            MaxRange += Random.Range(MaxRange/SysCount/2, MaxRange/SysCount/1.25f);
            Ylimiter += Random.Range(0, 0.1f);

            SysCount--;
        }
        StartCoroutine(ScatterResourceTimer());
    }

    void ScatterResources()
    {
        int totalSysSum = 0;
        foreach (StarSystem ss in FindObjectsOfType<StarSystem>())
        {
            if (recycledResources > totalSysSum * 10)
            {
                totalSysSum++;
                ss.GetComponent<StarSystem>().resources += 10;
            }
        }
        if (recycledResources > totalSysSum * 10) recycledResources -= totalSysSum * 10;
    }

    public IEnumerator ScatterResourceTimer()
    {
        ScatterResources();
        yield return new WaitForSeconds(60);
        StartCoroutine(ScatterResourceTimer());
    }
}
