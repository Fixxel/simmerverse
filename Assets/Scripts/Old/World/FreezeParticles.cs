﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeParticles : MonoBehaviour {

    public float freezeAfterSeconds;

	// Use this for initialization
	void Start () {
        StartCoroutine(freezeParticlesIn(freezeAfterSeconds));
	}

    private IEnumerator freezeParticlesIn(float time)
    {
        yield return new WaitForSeconds(time);
        GetComponent<ParticleSystem>().Pause();
    }
}
