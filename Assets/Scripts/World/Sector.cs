﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sector : Sectors {
	//this script is the functionality of a sector.
	//sector is set up by Sectors script.
	private float Resources;
	private int Planets;
	private int Stars;

	void Awake () {
		Resources = RollResources();
		Planets = RollPlanets();
		Stars = RollStars();
	}

	void Start()
	{
		
	}

	void OnTriggerEnter(Collider col)
	{
		
	}

	void OnTriggerStay(Collider col)
	{
		
	}

	void OnTriggerExit(Collider col)
	{
		
	}
}
