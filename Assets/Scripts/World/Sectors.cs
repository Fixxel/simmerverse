﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sectors : MonoBehaviour {
	
	//this script sets up sectors.
	public GameObject SectorRef;

	public float SectorCount;
	private bool HasPlanets;
	private float ResourceAmount;
	private int StarAmount;
	private int PlanetAmount;

	void Awake()
	{
		List<float> List3D = new List<float>() {SectorCount, SectorCount, SectorCount};

		//sectorcountxsectorcountxsectorcount	
/* 
		for (int num1 = 0; num1 < SectorCount; num1++){
			
			GameObject L = Instantiate(SectorRef);
			L.transform.position = new Vector3(List3D[0], List3D[1], List3D[2]);
			List3D[0]++;
			
			for (int num2 = 0; num2 < SectorCount; num2++){
				
				GameObject L2 = Instantiate(SectorRef);
				L2.transform.position = new Vector3(List3D[0], List3D[1], List3D[2]);
				List3D[1]++;

				for (int num3 = 0; num3 < SectorCount; num3++){

					GameObject L3 = Instantiate(SectorRef);
					L3.transform.position = new Vector3(List3D[0], List3D[1], List3D[2]);
					List3D[2]++;
				}
			}
		} */
	}

	public float RollResources(){
		ResourceAmount = Random.Range(0f, 100f);
		return ResourceAmount;
	}

	public int RollStars(){
		StarAmount = Random.Range(0, 2);
		return StarAmount;
	}

	public int RollPlanets(){
		PlanetAmount = Random.Range(0, 10);
		return PlanetAmount;
	}

}
