﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour
{

	//float test = 2f;
    // Update is called once per frame
    void Update(){

        //Debug.Log(test % 2);
	    //test += 0.1f;

        //camera rotation for Mobile
        #if UNITY_ANDROID

        if (Input.touchCount <= 1 && Input.GetTouch(0).position.y > Screen.height / 2 + 200)
        {
            transform.Translate(Vector3.up * Vector3.Distance(transform.position, Vector3.zero) / 2 * Time.deltaTime);
        }
        else if (Input.touchCount <= 1 && Input.GetTouch(0).position.y < Screen.height / 2 - 200)
        {
            transform.Translate(Vector3.down * Vector3.Distance(transform.position, Vector3.zero) / 2 * Time.deltaTime);
        }

        if (Input.touchCount <= 1 && Input.GetTouch(0).position.x > Screen.width / 2 + 200)
        {
            transform.Translate(Vector3.right * Vector3.Distance(transform.position, Vector3.zero) / 2 * Time.deltaTime);
        }
        else if (Input.touchCount <= 1 && Input.GetTouch(0).position.x < Screen.width / 2 - 200)
        {
            transform.Translate(Vector3.left * Vector3.Distance(transform.position, Vector3.zero) / 2 * Time.deltaTime);
        }

        if (Input.touchCount == 2)
        {
            if(Vector3.Distance(transform.position, Vector3.zero) > 0.2f) transform.Translate(Vector3.forward * Vector3.Distance(transform.position, Vector3.zero) / 2 * Time.deltaTime);
        }
        else if (Input.touchCount == 3)
        {
            transform.Translate(Vector3.back * Vector3.Distance(transform.position, Vector3.zero) / 2 * Time.deltaTime);
        }
        #endif

        //camera rotation for PC
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.up * Vector3.Distance(transform.position, Vector3.zero) / 2 * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.down * Vector3.Distance(transform.position, Vector3.zero) / 2 * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * Vector3.Distance(transform.position, Vector3.zero) / 2 * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * Vector3.Distance(transform.position, Vector3.zero) / 2 * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.E))
        {
            if(Vector3.Distance(transform.position, Vector3.zero) > 0.2f) transform.Translate(Vector3.forward * Vector3.Distance(transform.position, Vector3.zero) / 2 * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.Q))
        {
            transform.Translate(Vector3.back * Vector3.Distance(transform.position, Vector3.zero) / 2 * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            if (Screen.fullScreen) Screen.fullScreen = false;
            else if (!Screen.fullScreen) Screen.fullScreen = true;
        }
        
        transform.LookAt(Vector3.zero);
    }
}
